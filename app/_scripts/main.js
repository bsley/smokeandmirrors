var s = skrollr.init({forceHeight: false});

// INSTAGRAM API CALL

// $.ajax({
// 	type: "GET",
// 	dataType: "jsonp",
// 	cache: false,     
// 	url: "https://api.instagram.com/v1/users/36170336/media/recent/?access_token=1227236256.5c53f4a.77d8b85884924c3aa5027e2252e4e80a",
// 	success: function(media) {
//     c = 1;
//     output = '';
// 		for (var i in media.data) {
//       output += "<div class='swiper-slide'><img src='" + media.data[i].images.standard_resolution.url +"' /></div>";
// 		}
//     output += ''
//     $("#swiper-wrapper").append(output);
// 	}
// });

// if (document.documentElement.clientWidth > 990) {

//   $('.playground').waypoint(function(direction) {
//     $('section.hero').toggleClass('switch');
//     console.log('playground hit!');
//   }, { 
//     offset: "0%",
//   });

// }

// INITIALIZE SWIPER

var mySwiper = new Swiper('.swiper-container',{
pagination: '.pagination',
paginationClickable: true,
// centeredSlides: true,
slidesPerView: 3,
// loop: true,
keyboardControl: true,
// watchActiveIndex: true
});

// RANDOMLY SPACE INSTAGRAM FEED

var max = 100;
var min = 1;
var rmax = 10;
var rmin = 1;

var div = document.getElementById('swiper-wrapper'),
divChildren = div.getElementsByTagName('div');

// console.log(divChildren);

for (var i=0; i<divChildren.length; i++) {
    var randomInt = Math.random() * (min - max) + min;
    // divChildren[i].css("top", randomInt + "px");
    divChildren[i].style.webkitTransform = "rotate(randomInt+'deg')";
    divChildren[i].style.marginTop = randomInt+'px';
    // divChildren[i].css({"transform","translateY( randomInt + 'px' )"});
};


/* SCROLLING FUNCTION FOR "CONTACT" LINK */

function goToByScroll(id){
  // Remove "link" from the ID
id = id.replace("Link", "");
  // Scroll
$('html,body').animate({
    scrollTop: $("#"+id).offset().top},
    'fast');
}

$("header a.scroll").click(function(e) { 
      // Prevent a page reload when a link is pressed
    e.preventDefault(); 
      // Call the scroll function
    goToByScroll($(this).attr("id"));  
});

// BOTTOM LOGO SCROLLS TO TOP OF page

$(".logomark").click(function() {
	$("html, body").animate({ scrollTop: 0 }, 100);
	return false;
});

// TOP SLIDESHOW

// var photoSet = [
// "_img/slide_01.jpg",
// "_img/slide_02.jpg",
// "_img/slide_03.jpg"
// ];

// var p = photoSet.length;
// console.log(p);
// var i = 1;

// function loopPhotos() {
//   setTimeout(function() {
//     $('.hero').css({'background-image': 'url(' + photoSet[i] + ')',});
//     i++;
//     console.log(i);
//     if (i < p) {
//       loopPhotos();
//     } else {
//       i = 0;
//       loopPhotos();
//     }
//   }, 5000)
// }

var photoSet = [
"_img/slide_01.jpg",
"_img/slide_02.jpg",
"_img/slide_03.jpg"
];
var p = photoSet.length - 1;
var i = 1;
var z = 0;
var original = document.getElementById("item");

function loopPhotos() {
  setTimeout(function() {
    var clone = original.cloneNode(true);
    clone.id = "item " + ++z;
    original.parentNode.appendChild(clone);
    $(clone).css({
      'background-image': 'url(' + photoSet[i] + ')',
    });
    $(clone).velocity("transition.fadeIn", { 
        duration: 1200
    });
    i++;
    console.log(i);
    
    // call the next indexed image and begin to preload it
    var img = new Image();
    img.src = photoSet[i];
    
    if (i > p) {
      i = 0;
      // stop preloading images, somehow used images that are cached so the browser isn't working eternally reloading the same X number of photos.
    }
    loopPhotos();
  }, 5000)
}

$( document ).ready(function() {

  // load first image
  var firstImg = new Image();
  firstImg.src = photoSet[0];
  $('.item:nth-child(1)').css({
    'background-image': 'url(' + firstImg.src + ')',
  });
  $('.item:nth-child(1)').css("opacity", "1");
  
  // when first image is loaded, fade it in and begin looping
  firstImg.onload = function() {

      $('.myslideshow').velocity("transition.fadeIn", { 
            duration: 1500
      });

      console.log('first image loaded!');
      var secondImg = new Image();
      secondImg.src = photoSet[1];
      loopPhotos();

      $('.bigLogo svg g').velocity("transition.fadeIn", { 
        duration: 200,
        stagger: 100,
        delay: 800,
      });

      $('nav ul').velocity("transition.slideDownIn", { 
        duration: 500,
        delay: 2500,
      });
  };

  // VELOCITY ANIMATION EVENTS

  

  

});







